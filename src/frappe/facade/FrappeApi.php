<?php

namespace frappe\facade;

use think\Facade;

/**
 * Class FrappeApi
 * @package frappe\facade
 * @mixin \frappe\FrappeApi
 */
class FrappeApi extends Facade
{
    protected static function getFacadeClass()
    {
        return \frappe\FrappeApi::class;
    }
}
