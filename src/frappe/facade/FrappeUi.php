<?php

namespace frappe\facade;

use think\Facade;

/**
 * Class FrappeUi
 * @package frappe\facade
 * @mixin \frappe\FrappeUi
 */
class FrappeUi extends Facade
{
    protected static function getFacadeClass()
    {
        return \frappe\FrappeUi::class;
    }
}
