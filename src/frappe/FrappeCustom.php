<?php

namespace frappe;

use think\App;
use think\Request;

abstract class FrappeCustom
{
    /**
     * @var \app\Request|Request
     */
    protected $request;
    /**
     * @var App|mixed|object
     */
    protected $app;

    public function __construct()
    {
        $this->app = app();
        $this->request = request();
    }

    abstract public function handle();
}