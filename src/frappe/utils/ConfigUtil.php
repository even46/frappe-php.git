<?php

namespace frappe\utils;

use Exception;
use InvalidArgumentException;
use think\Collection;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Db;
use think\Model;

class ConfigUtil
{
    public static function getConfigByName(string $type, string $name, bool $yes = true): array
    {
        $tableName = self::getTableName($type);
        $condition = [];
        if ($yes) $condition[] = ['status', '=', 'YES'];
        $condition[] = ['name', '=', $name];
        $condition[] = ['type', '<>', 'GROUP'];
        $config = Db::table($tableName)->where($condition)->find();
        if (!$config) throw new DataNotFoundException("Data Not Found!");
        if (isset($config['type'])) {
            $config['type'] = strtoupper($config['type'] ?? "");
        }
        if (isset($config['config'])) {
            $config['config'] = $config['config'] ? json_decode($config['config'], true) : null;
        }
        if (isset($config['children'])) {
            $config['children'] = json_decode($config['children'] ?? "", true);
        }
        if (isset($config['component'])) {
            $config['component'] = json_decode($config['component'] ?? "", true);
        }
        return $config;
    }

    public static function getConfig(string $type, string $id, bool $yes = true): array
    {
        $tableName = self::getTableName($type);
        $condition = [];
        if ($yes) $condition[] = ['status', '=', 'YES'];
        $config = Db::table($tableName)->where($condition)->find($id);
        if (!$config) throw new DataNotFoundException("table data not Found");
        $config['config'] = $config['config'] ? json_decode($config['config'], true) : null;
        $config['type'] = strtoupper($config['type'] ?? "");
        return $config;
    }



    /**
     * 获取API配置
     * @param string $id API数据ID
     * @return array|mixed|Db|Model
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     * @author yinxu
     * @date 2024/3/23 10:32:03
     */
    public static function getApiConfig(string $id, bool $yes = true): array
    {
        $tableName = self::getTableName('api');
        $condition = [];
        if ($yes) $condition[] = ['status', '=', 'YES'];
        $config = Db::table($tableName)->where($condition)->find($id);
        if (!$config) throw new DataNotFoundException("table data not Found");
        $config['config'] = $config['config'] ? json_decode($config['config'], true) : null;
        $config['type'] = strtoupper($config['type'] ?? "");
        return $config;
    }

    /**
     * 获取API配置列表
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public static function getApiList()
    {
        $tableName = self::getTableName('api');
        $configs = Db::table($tableName)
            ->where('status', 'YES')
            ->order('id desc')
            ->select();
        if ($configs instanceof Collection) {
            $configs->each(function ($config) {
                $config['config'] = $config['config'] ? json_decode($config['config'], true) : null;
                $config['type'] = strtoupper($config['type'] ?? "");
                return $config;
            });
        }
        return $configs;
    }

    /**
     * 新增API
     * @param array $config
     * @return int|string
     * @author yinxu
     * @date 2024/3/23 17:22:53
     */
    public static function createApi(array $config)
    {
        $config['config'] = json_encode($config['config'] ?? [], 320);
        $config['client'] = $config['client'] ? implode(',', $config['client']) : '';
        return Db::table(self::getTableName('api'))->insert($config);
    }

    /**
     * 更新API
     * @param string $id
     * @param array $config
     * @return int
     * @throws DbException
     * @author yinxu
     * @date 2024/3/23 17:24:24
     */
    public static function updateApi(string $id, array $config): int
    {
        $config['config'] = json_encode($config['config'] ?? [], 320);
        $config['client'] = $config['client'] ? implode(',', $config['client']) : '';
        return Db::table(self::getTableName('api'))->where('id', $id)->update($config);
    }

    /**
     * 删除API
     * @param string $id
     * @return int
     * @throws DbException
     * @author yinxu
     * @date 2024/3/23 17:25:17
     */
    public static function deleteApi(string $id): int
    {
        return Db::table(self::getTableName('api'))->where('id', $id)->delete();
    }

    /**
     * 获取数据表名
     * @param string $name
     * @return mixed
     * @author yinxu
     * @date 2024/3/23 17:07:41
     */
    public static function getTableName(string $name)
    {
        $tableName = config("frappe.table.$name", '');
        if (empty($tableName)) throw new InvalidArgumentException("Frappe Config Error: Invalid Table Name");
        return $tableName;
    }

    /**
     * 查询数据表信息
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     * @author yinxu
     * @date 2024/3/24 18:36:31
     */
    public static function querySchemaTables(): array
    {
        return Db::table('INFORMATION_SCHEMA.TABLES')
            ->field('TABLE_NAME as tableName, TABLE_COMMENT as tableTitle')
            ->where('TABLE_SCHEMA', config('database.connections.' . config('database.default') . '.database', ''))
            ->select()
            ->toArray();
    }

    /**
     * 查询数据表字段信息
     * @param string $tableName
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     * @author yinxu
     * @date 2024/3/24 18:36:37
     */
    public static function querySchemaTableColumns(string $tableName): array
    {
        return Db::table('INFORMATION_SCHEMA.COLUMNS')
            ->field('TABLE_NAME as tableName, COLUMN_NAME as columnName, COLUMN_COMMENT as columnTitle, ORDINAL_POSITION as weight, COLUMN_DEFAULT as columnDefault, DATA_TYPE as dataType, CHARACTER_MAXIMUM_LENGTH as charLength, NUMERIC_PRECISION as columnLength, NUMERIC_SCALE as columnPrecision, COLUMN_KEY as columnKey')
            ->where('TABLE_SCHEMA', config('database.connections.' . config('database.default') . '.database', ''))
            ->where('TABLE_NAME', $tableName)
            ->select()
            ->toArray();
    }

    public static function getFrappePathName()
    {
        return config('frappe.path', 'frappe');
    }

    /**
     * 获取模版内容
     * @return string
     * @throws Exception
     * @author yinxu
     * @date 2024/3/23 18:00:23
     */
    public static function getTemplateContent(): string
    {
        $file = __DIR__ . '/../../tpl/template.tpl';
        if (!is_file($file)) {
            throw new Exception("Missing Template File!");
        }
        $content = file_get_contents($file);
        if (!$content) {
            throw new Exception("Template File Has No Content!");
        }
        return $content;
    }
}