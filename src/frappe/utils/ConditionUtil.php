<?php

namespace frappe\utils;

class ConditionUtil
{
    /**
     * @var string 主数据表名
     */
    protected $tableName;
    /**
     * @var array 查询条件
     */
    protected $conditions = [];
    /**
     * @var array 查询参数
     */
    protected $queryParams = [];
    /**
     * @var array 默认查询参数
     */
    protected $defaultParams = [];
    /**
     * @var array 固定查询参数
     */
    protected $fixedPrams = [];
    /**
     * @var array 必须字段
     */
    protected $requiredParams = [];
    /**
     * @var array 缓存条件
     */
    protected $_CacheConditions = [];
    /**
     * @var array where查询集合
     */
    protected $wheres = [];
    /**
     * @var array RawWhere查询集合
     */
    protected $rawWheres = [];

    /**
     * 构造
     * @param string $tableName 数据表名
     * @param array $conditions 查询条件
     * @param array $queryParams 查询参数
     * @param array $defaultParams 默认参数
     * @param array $fixedParams 固定参数
     */
    public function __construct(string $tableName, array $conditions = [], array $queryParams = [], array $defaultParams = [], array $fixedParams = [], array $requiredParams = [])
    {
        $this->tableName = $tableName;
        $this->conditions = $conditions;
        $this->queryParams = $queryParams;
        $this->defaultParams = $defaultParams;
        $this->fixedPrams = $fixedParams;
        $this->requiredParams = $requiredParams;
    }

    /**
     * 加载配置
     * @param string $tableName
     * @param array $conditions
     * @param array $queryParams
     * @param array $defaultParams
     * @param array $fixedParams
     * @return ConditionUtil
     */
    public static function load(string $tableName, array $conditions = [], array $queryParams = [], array $defaultParams = [], array $fixedParams = [], array $requiredParams = []): ConditionUtil
    {
        return new ConditionUtil($tableName, $conditions, $queryParams, $defaultParams, $fixedParams, $requiredParams);
    }

    /**
     * 构建条件
     * @param bool $hasAlias
     * @return array
     */
    public function build(bool $hasAlias = true): array
    {
        # 第一步解析查询条件
        $this->convertConditions()
            # 第二步对参数进行组合
            ->mergeQueryParams()
            ->checkRequired()
            # 第三步解析查询参数
            ->parseQueryParams($hasAlias);
        # 返回数据
        return [$this->wheres, $this->rawWheres];
    }

    protected function checkRequired(): ConditionUtil
    {
        foreach ($this->requiredParams as $name) {
            if (!isset($this->queryParams[$name])) {
                throw new \Exception('参数错误');
            }
        }
        return $this;
    }

    protected function mergeQueryParams(): ConditionUtil
    {
        # 合并默认参数值
        foreach ($this->defaultParams as $defKey => $defValue) {
            if (empty($defKey) || isset($this->queryParams[$defKey])) continue;
            $this->queryParams[$defKey] = $defValue;
        }
        # 合并固定参数值
        foreach ($this->fixedPrams as $fixedKey => $fixedValue) {
            $this->queryParams[$fixedKey] = $fixedValue;
        }
        return $this;
    }

    protected function convertConditions(): ConditionUtil
    {
        foreach ($this->conditions as $condition) {
            if (!isset($condition['name'])) continue;
            if (!isset($condition['option'])) continue;
            $this->_CacheConditions[$condition['name']] = $condition;
        }
        return $this;
    }

    /**
     * 解析查询字段
     * @return $this
     */
    protected function parseQueryParams(bool $hasAlias): ConditionUtil
    {
        if (!$this->conditions) return $this;
        if (!$this->queryParams) return $this;
        foreach ($this->queryParams as $field => $value) {
            $this->parseQueryParam($field, $value, $hasAlias);
        }
        return $this;
    }

    /**
     * 解析查询字段
     * @param string $field
     * @param mixed $value
     * @param bool $hasAlias
     */
    protected function parseQueryParam(string $field, $value, bool $hasAlias)
    {
        if (is_string($value)) $value = trim($value);
        if (!isset($this->_CacheConditions[$field])) return;
        $options = $this->_CacheConditions[$field];
        if ($hasAlias) {
            $raw = isset($options['raw']) && !empty($options['raw']) ? $options['raw'] : $this->tableName . "." . $field;
            if (strpos($raw, ".") == false) {
                $raw = $this->tableName . "." . $raw;
            }
        } else {
            $raw = $options['raw'] ?? $field;
        }

        $option = $options['option'] ?: '=';

        switch ($option) {
            case '=':
            case 'eq':
                if (is_string($value) && $value == "") break;
                $this->wheres[] = [$raw, '=', $value];
                break;
            case '<':
            case '>':
            case '<=':
            case '>=':
                if (is_null($value) || $value == "") break;
                $this->wheres[] = [$raw, $option, $value];
                break;
            case 'year':
                $this->rawWheres[] = "year($raw) = $value";
                break;
            case 'left_like':
                $this->wheres[] = [$raw, 'like', $value . '%'];
                break;
            case 'right_like':
                $this->wheres[] = [$raw, 'like', '%' . $value];
                break;
            case 'like':
                $this->wheres[] = [$raw, 'like', '%' . $value . '%'];
                break;
            case 'in':
                if (is_string($value) && strpos($raw, ",") != false) $value = explode(',', $value);
                if (is_array($value)) {
                    $this->wheres[] = [$raw, 'in', $value];
                }
                break;
            case 'between':
                if (is_array($value) && count($value) == 2) {
                    $this->wheres[] = [$raw, 'between', $value];
                }
                break;
            case 'date_range':
                if (is_array($value) && count($value) == 2) {
                    $this->wheres[] = [$raw, 'between', [date('Y-m-d', strtotime($value[0])), date('Y-m-d', strtotime($value[1]))]];
                }
                break;
            case 'datetime_range':
                if (is_array($value) && count($value) == 2) {
                    $this->wheres[] = [$raw, 'between', [date('Y-m-d H:i:s', strtotime($value[0])), date('Y-m-d H:i:s', strtotime($value[1]))]];
                }
                break;
            default:
                $this->wheres[] = [$raw, $options['option'], $value];
        }
    }
}