<?php

namespace frappe;

use frappe\api\FrappeCreate;
use frappe\api\FrappeCustom;
use frappe\api\FrappeDelete;
use frappe\api\FrappeGet;
use frappe\api\FrappeOption;
use frappe\api\FrappeSelect;
use frappe\api\FrappeUpdate;
use frappe\entity\ApiEntity;
use frappe\entity\CreateApiEntity;
use frappe\entity\CustomApiEntity;
use frappe\entity\DeleteApiEntity;
use frappe\entity\GetApiEntity;
use frappe\entity\OptionApiEntity;
use frappe\entity\SelectApiEntity;
use frappe\entity\UpdateApiEntity;
use frappe\utils\ConfigUtil;
use frappe\utils\CustomUtil;
use InvalidArgumentException;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Config;
use think\Request;

class FrappeApi
{
    /**
     * @var \app\Request|Request
     */
    protected $request;
    /**
     * @var ApiEntity
     */
    protected $apiEntity;

    public function __construct(Config $config, Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param string $name
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function load(string $name)
    {
        $config = ConfigUtil::getConfigByName('api', $name);
        $this->apiEntity = new ApiEntity($config);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function execute(string $name)
    {
        // 加载配置数据
        $this->load($name);

        // 执行数据
        switch ($this->apiEntity->type) {
            case 'SELECT':
                return FrappeSelect::load($this->request, $this->apiEntity->config)->select();
            case 'GET':
                return FrappeGet::load($this->request, $this->apiEntity->config)->get();
            case 'CREATE':
                return FrappeCreate::load($this->request, $this->apiEntity->config)->insert();
            case 'UPDATE':
                return FrappeUpdate::load($this->request, $this->apiEntity->config)->update();
            case 'DELETE':
                return FrappeDelete::load($this->request, $this->apiEntity->config)->delete();
            case 'OPTION':
                return FrappeOption::load($this->request, $this->apiEntity->config)->get();
            case 'CUSTOM':
                return FrappeCustom::load($this->request, $this->apiEntity->config)->run();
            default:
                throw new InvalidArgumentException("{$this->apiEntity->title} 配置无效");
        }
    }
}

