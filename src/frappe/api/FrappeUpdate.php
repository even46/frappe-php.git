<?php

namespace frappe\api;

use frappe\entity\UpdateApiEntity;
use frappe\utils\ConditionUtil;
use frappe\utils\ConvertUtil;
use InvalidArgumentException;
use think\db\exception\DbException;
use think\db\Query;
use think\facade\Db;
use think\Request;

class FrappeUpdate
{
    /**
     * @var \app\Request|Request
     */
    protected $request;
    /**
     * @var Query|Db
     */
    protected $db;
    /**
     * @var UpdateApiEntity
     */
    protected $entity;
    /**
     * @var array Post 数据
     */
    protected $postData = [];
    /**
     * @var array Update 数据
     */
    protected $updateData = [];
    /**
     * @param \app\Request|Request $request
     * @param array $config
     */
    public function __construct(Request $request, array $config)
    {
        $this->request = $request;
        $this->entity = new UpdateApiEntity($config);
        $this->db = Db::name($this->entity->tableName)->alias($this->entity->tableName);
    }

    /**
     * 加载配置
     * @param Request $request
     * @param array $config
     * @return FrappeUpdate
     */
    public static function load(Request $request, array $config): FrappeUpdate
    {
        return new FrappeUpdate($request, $config);
    }

    /**
     * 执行更新
     * @return int
     * @throws DbException
     * @author yinxu
     * @date 2024/3/23 12:10:09
     */
    public function update(): int
    {
        $this->postData = $this->request->param();
        # 解析查询参数
        $this->entity->setQueryParams($this->postData);
        # 解析更新参数
        $this->entity->fixedParams = ConvertUtil::convertByGlobal($this->entity->fixedParams);
        $this->entity->fixedData = ConvertUtil::convertByGlobal($this->entity->fixedData);
        # 顺序：1-请求数据 2-合并默认数据-覆盖固定数据 4-字段数据验证 5-更新数据
        $this->where()->mergeData()->rebuildData();
        $res = $this->db->update($this->updateData);
        // Event
        if ($res && $this->entity->afterEvents) {
            foreach ($this->entity->afterEvents as $afterEvent) {
                event($afterEvent);
            }
        }
        return $res;
    }

    /**
     * 合并数据
     * @return $this
     * @author yinxu
     * @date 2024/3/23 12:09:49
     */
    protected function mergeData(): FrappeUpdate
    {
        # 合并默认参数值
        foreach ($this->entity->defaultData as $defKey => $defValue) {
            if (empty($defKey) || isset($this->postData[$defKey])) continue;
            $this->postData[$defKey] = $defValue;
        }
        # 合并固定参数值
        foreach ($this->entity->fixedData as $fixedKey => $fixedValue) {
            $this->postData[$fixedKey] = $fixedValue;
        }
        return $this;
    }

    /**
     * 重组&校验数据
     * @return $this
     * @author yinxu
     * @date 2024/3/23 12:10:00
     */
    protected function rebuildData(): FrappeUpdate
    {
        $this->updateData = [];
        foreach ($this->entity->tableFields as $field) {
            $name = $field['name'] ?? "";
            $raw = $field['raw'] ?? "";
            $type = $field['type'] ?? "";
            $convert = $field['convert'] ?? "";
            $required = $field['required'] ?? false;
            $unique = $field['unique'] ?? false;
            if (empty($name) || empty($type)) continue;
            if ($required && !isset($this->postData[$name])) throw new InvalidArgumentException("缺少[$name]参数");
            if (!isset($this->postData[$name])) continue;
            $value = $this->postData[$name];
            # 转换数据格式
            $value = ConvertUtil::convert($convert, $value);
            # TODO：验证数据

            # 数据重新给到 Update Data
            $this->updateData[$name] = $value;
        }
        return $this;
    }

    /**
     * 查询数据
     * @return $this
     */
    protected function where(): FrappeUpdate
    {
        [$wheres, $rawWheres] = ConditionUtil::load($this->entity->tableName, $this->entity->conditions, $this->entity->queryParams, $this->entity->defaultParams, $this->entity->fixedParams)->build();
        if ($wheres) $this->db = $this->db->where($wheres);
        foreach ($rawWheres as $rawWhere) {
            $this->db = $this->db->whereRaw($rawWhere);
        }
        return $this;
    }




}