<?php
declare (strict_types=1);

namespace frappe\api;

use frappe\entity\GetApiEntity;
use frappe\utils\ConditionUtil;
use frappe\utils\ConvertUtil;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\db\Query;
use think\facade\Db;
use think\facade\Log;
use think\Request;

/**
 * 单条数据查询
 */
class FrappeGet
{
    /**
     * @var \app\Request|Request
     */
    protected $request;
    /**
     * 数据库查询对象
     * @var Query|Db
     */
    protected $db;
    /**
     * @var GetApiEntity
     */
    protected $entity;
    /**
     * 数据库查询原始数据
     * @var mixed
     */
    public $originalData;
    /**
     * 响应数据
     * @var mixed
     */
    public $responseData;
    /**
     * 缓存字段数据
     * @var array
     */
    public $_CacheTableFields = [];
    /**
     * 缓存字段需转换的配置
     * @var array
     */
    public $_CacheTableFieldConverts = [];

    /**
     * 构造数据
     * @param array $config 配置参数
     */
    public function __construct(Request $request, array $config)
    {
        $this->request = $request;
        $this->entity = new GetApiEntity($config);
        $this->db = Db::name($this->entity->tableName)->alias($this->entity->tableName);
    }

    /**
     * 加载配置
     * @param Request $request
     * @param array $config
     * @return FrappeGet
     */
    public static function load(Request $request, array $config): FrappeGet
    {
        return new FrappeGet($request, $config);
    }

    /**
     * 执行查询
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function get()
    {
        $this->entity->queryParams = $this->request->param() ?? [];
        $this->entity->queryOrder = $this->request->param('orders/a', []);
        $this->entity->fixedParams = ConvertUtil::convertByGlobal($this->entity->fixedParams);
        $this->joins()->fields()->where()->order();
        $this->originalData = $this->db->find();
        $this->convertData();
        return $this->responseData;
    }

    /**
     * Join关联查询表
     * @return $this
     */
    protected function joins(): FrappeGet
    {
        foreach ($this->entity->tableJoins as $join) {
            $joinName = $join['name'];
            $joinAlias = $join['alias'] ?? $joinName;
            $joinCondition = $join['condition'];
            $joinType = $join['type'] ?? "left";
            $this->db = $this->db->join("$joinName $joinAlias", $joinCondition, $joinType);
        }
        return $this;
    }

    /**
     * 获取查询字段
     * @return $this
     */
    protected function fields(): FrappeGet
    {
        $this->_CacheTableFields = [];
        foreach ($this->entity->tableFields as $field) {
            $name = $field['name'] ?? "";
            $raw = $field['raw'] ?? "";
            $convert = $field['convert'] ?? "";

            if (empty($name)) continue;
            if ($raw) {
                $field = "$raw as $name";
            } else {
                $field = $this->entity->tableName . "." . $name;
            }

            # 查询字段
            $this->db = $raw ? $this->db->fieldRaw($field) : $this->db->field($field);
            $this->_CacheTableFields[] = $field;
            if ($convert) $this->_CacheTableFieldConverts[$name] = $convert;
        }
        return $this;
    }

    /**
     * 查询数据
     * @return $this
     */
    protected function where(): FrappeGet
    {
        [$wheres, $rawWheres] = ConditionUtil::load($this->entity->tableName, $this->entity->conditions, $this->entity->queryParams, $this->entity->defaultParams, $this->entity->fixedParams)->build();
        if ($wheres) $this->db = $this->db->where($wheres);
        foreach ($rawWheres as $rawWhere) {
            $this->db = $this->db->whereRaw($rawWhere);
        }
        return $this;
    }

    /**
     * 字段排序查询
     * @return $this
     */
    protected function order(): FrappeGet
    {
        $queryOrders = $this->entity->queryOrder ?? $this->entity->defaultOrder ?? [];
        foreach ($queryOrders as $queryOrder) {
            $this->db = $this->db->order($queryOrder['name'], strtolower(trim($queryOrder['sort'])) == "desc" ? "desc" : "asc");
        }
        return $this;
    }

    /**
     * 转换返回数据
     * @return $this
     */
    protected function convertData(): FrappeGet
    {
        $this->responseData = $this->originalData ?? [];
        foreach ($this->_CacheTableFieldConverts as $name => $convert) {
            if (!isset($this->responseData[$name])) continue;
            $this->responseData[$name] = ConvertUtil::convert($convert, $this->responseData[$name]);
        }
        # 在转Tree结构
        return $this;
    }
}