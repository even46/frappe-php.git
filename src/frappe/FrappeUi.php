<?php

namespace frappe;

use frappe\api\FrappeCreate;
use frappe\api\FrappeCustom;
use frappe\api\FrappeDelete;
use frappe\api\FrappeGet;
use frappe\api\FrappeOption;
use frappe\api\FrappeSelect;
use frappe\api\FrappeUpdate;
use frappe\entity\ApiEntity;
use frappe\entity\CreateApiEntity;
use frappe\entity\CustomApiEntity;
use frappe\entity\DeleteApiEntity;
use frappe\entity\GetApiEntity;
use frappe\entity\OptionApiEntity;
use frappe\entity\SelectApiEntity;
use frappe\entity\UpdateApiEntity;
use frappe\utils\ConfigUtil;
use frappe\utils\CustomUtil;
use InvalidArgumentException;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Config;
use think\Request;

class FrappeUi
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @param string $id
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function load(string $type, string $id)
    {
        $this->config = ConfigUtil::getConfigByName($type, $id);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function execute(string $type, string $id)
    {
        if (!in_array($type, ['page', 'component'])) {
            throw new \InvalidArgumentException('参数无效');
        }
        $this->load($type, $id);
        return $this->config;
    }
}

