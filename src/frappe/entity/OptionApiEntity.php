<?php

namespace frappe\entity;

use frappe\utils\ConvertUtil;
use think\facade\Validate;

class OptionApiEntity
{
    /**
     * @var string static | dynamic
     */
    public $type = 'static';
    /**
     * @var array 数据
     */
    public $options = [];
    /**
     * @var array 配置
     */
    public $config = [];

    /**
     * 配置验证规则
     * @var array
     */
    private $rules = [
        'type' => 'require|in:static,dynamic',
    ];
    /**
     * 配置验证错误提示
     * @var array
     */
    private $messages = [
        'type' => '类型错误',
    ];

    public function __construct(array $config)
    {
        Validate::rule($this->rules)->message($this->messages)->failException()->check($config);
        $this->type = $config['type'] ?? "static";
        if ($this->type == "static") {
            $this->options = $config['options'] ?? [];
        }
        if ($this->type == "dynamic") {
            $this->config = $config['config'] ?? [];
        }
        $this->parseOptions();
    }

    protected function parseOptions()
    {
        foreach ($this->options as &$option) {
            $option['value'] = ConvertUtil::convertType($option['type'] ?? 'text', $option['value'] ?? '');
        }
    }

    public function toArray(): array
    {
        return [
            'type' => $this->type,
            'options' => $this->options,
            'config' => $this->type == "dynamic" ? (new SelectApiEntity($this->config))->toArray() : [],
        ];
    }
}