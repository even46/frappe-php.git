<?php

namespace frappe\entity;

use think\facade\Validate;

class TableComponentEntity
{
    /**
     * Key
     * @var string
     */
    public $rowKey;
    /**
     * 请求接口
     * @var string
     */
    public $request;
    /**
     * 搜索配置
     * @var array
     */
    public $search = ['columns' => []];
    /**
     * Table列配置
     * @var array
     */
    public $columns = [];
    /**
     * 列表Row操作配置
     * @var array
     */
    public $rowActions = [];
    /**
     * Tabs配置
     * @var array
     */
    public $tabs = [];
    /**
     * 工具栏配置
     * @var array
     */
    public $toolbar = [];
    /**
     * 批量操作配置
     * @var array
     */
    public $batchActions = [];
    /**
     * 分页查询
     * @var bool
     */
    public $isPaginate = true;
    /**
     * 分页配置
     * @var bool
     */
    public $pagination = [
        'config' => [
            'pageSize' => 10,
            'pageSizeOptions' => [10, 30, 50, 100]
        ]
    ];
    /**
     * 配置验证规则
     * @var array
     */
    private $rules = [
        'tableName' => 'require',
    ];
    /**
     * 配置验证错误提示
     * @var array
     */
    private $messages = [
        'tableName' => '配置错误',
    ];

    public function __construct(array $config)
    {
        Validate::rule($this->rules)->message($this->messages)->failException()->check($config);
        $this->rowKey = $config['rowKey'] ?? "";
        $this->request = $config['request'] ?? "";
        $this->search = $config['search'] ?? [];
        $this->columns = $config['columns'] ?? [];
        $this->rowActions = $config['rowActions'] ?? [];
        $this->tabs = $config['tabs'] ?? [];
        $this->toolbar = $config['toolbar'] ?? [];
        $this->batchActions = $config['batchActions'] ?? "";
        $this->isPaginate = $config['isPaginate'] ?? true;
        $this->isTree = $config['isTree'] ?? false;
    }

    public function toArray(): array
    {
        return [
            'tableName' => $this->tableName,
            'defaultParams' => $this->defaultParams,
            'fixedParams' => $this->fixedParams,
            'conditions' => $this->conditions,
            'tableFields' => $this->tableFields,
            'tableJoins' => $this->tableJoins,
            'defaultOrder' => $this->defaultOrder,
            'tableGroup' => $this->tableGroup,
            'isPaginate' => $this->isPaginate,
            'isTree' => $this->isTree,
        ];
    }
}