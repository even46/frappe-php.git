<?php

namespace app\frappe\lib\constants;

class FormValueType
{
    # 文本
    const text = "text";
    # 多行文本
    const textarea = "textarea";
    # 数字
    const digit = "digit";
    # 数字区间
    const digitRange = "digitRange";
    # 密码
    const password = "password";
    # 日期
    const date = "date";
    # 时间
    const time = "time";
    # 日期时间
    const dateTime = "dateTime";
    # 日期区间
    const dateRange = "dateRange";
    # 时间区间
    const timeRange = "timeRange";
    # 日期时间区间
    const dateTimeRange = "dateTimeRange";
    # 选择
    const select = "select";
    # 树选择
    const treeSelect = "treeSelect";
    # 多选
    const checkbox = "checkbox";
    # 单选
    const radio = "radio";
    # 颜色
    const color = "color";
    # 开关
    const switch = "switch";
    # 长度
    const slider = "slider";
    # 评分
    const rate = "rate";
    # 分段
    const segmented = "segmented";
    # 金额
    const money = "money";
    # json
    const jsonCode = "jsonCode";
}