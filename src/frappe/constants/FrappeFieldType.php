<?php

namespace app\frappe\lib\constants;

use JetBrains\PhpStorm\Pure;

/**
 * 字段类型集
 */
class FrappeFieldType
{
    protected string $_apiType;
    protected string $_configType;

    # NAME
    public ?string $name = null;

    public ?string $alias = null;
    # Raw
    public ?string $raw = null;
    # 标签
    public ?string $label = null;
    # 类型
    public ?string $type = null;
    # 格式
    public ?string $format = null;
    # options
    public ?string $options = null;
    # 转换
    public ?string $convert = null;

    # ValueType: digit(数字类型)
    public ?string $valueType = null;

    # 构造
    public function __construct(string $apiType, string $configType)
    {
        $this->_apiType = strtoupper($apiType);
        $this->_configType = strtoupper($configType);
    }

    #[Pure]
    public static function load(string $apiType, string $configType): FrappeField
    {
        return new FrappeField($apiType, $configType);
    }

    #[Pure]
    public function toArray(): array
    {
        $data = [];
        $arrowFields = $this->getAllowFields();
        foreach ($arrowFields as $field) {
            if (!is_null($this->$field)) {
                $data[$field] = $this->$field;
            }
        }
        return $data;
    }

    protected function getAllowFields(): array
    {
        return match ($this->_apiType . '.' . $this->_configType) {
            "SELECT.FIELD" => ['name', 'type', 'raw', 'label', 'options', 'convert'],
            default => [],
        };
    }
}