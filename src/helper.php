<?php
declare(strict_types=1);

use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

if (!function_exists('frappe_api')) {
    function frappe_api(string $name)
    {
        return \frappe\facade\FrappeApi::execute($name);
    }
}

if (!function_exists('frappe_ui')) {
    function frappe_ui(string $type, string $name)
    {
        return \frappe\facade\FrappeUi::execute($type, $name);
    }
}

if (!function_exists('frappe_dev')) {
    function frappe_dev(string $action)
    {
        return \frappe\facade\FrappeDev::action($action);
    }
}