<?php

namespace app\frappe;

use frappe\FrappeCustom;

class {CLASS_NAME} extends FrappeCustom
{
    public function handle()
    {
        {FUNCTION_CONTENT}
    }
}