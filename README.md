
# frappe-php

thinkphp6｜thinkphp8 Json To Api

## 安装
> composer require yinxu46/frappe-php



## 使用

### 路由中使用

~~~
Route::rule('/api.web.v1/:__id$',function (Request $request, string $__id) {
    try {
        $response = frappe_api($__id);
        return api_success($response);
    }catch (\Throwable $throwable) {
        return api_error($throwable);
    }
})->middleware([
    # 设置中间键
]);
~~~